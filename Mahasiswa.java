/**
 * @author  : Fajar Subhan
 * @desc    : Hitung nilai mahasiswa on java
 * @npm     : 202043500578
 * 
 * */

package com.mahasiswa;

import javax.swing.JOptionPane;

public class Mahasiswa
{
    public static void main(String[] data)
    {
        /* ------- Deklarastion variable & assigment default value ------- */
        int n_tugas = 0,n_uas = 0,n_uts = 0,total = 0;

        /* Input Nilai */
        n_tugas = Integer.parseInt(JOptionPane.showInputDialog("Masukan nilai tugas"));
        n_uas   = Integer.parseInt(JOptionPane.showInputDialog("Masukan nilai uts"));
        n_uts   = Integer.parseInt(JOptionPane.showInputDialog("Masukan nilai uas"));

        // total nilai
        total = n_tugas + n_uas + n_uts;

        /* jika nilai lebih dari 80 */
        if(total > 80)
        {
            JOptionPane.showMessageDialog(null,"Nilai Anda " + total + " : A");
        }
        /* jika nilai lebih dari 70 */
        else if(total > 70)
        {
            JOptionPane.showMessageDialog(null,"Nilai Anda " + total +" : B");
        }
        /* jika nilai lebih dari 60 atau lebih dari 60 */
        else if(total > 60 || total > 50)
        {
            JOptionPane.showMessageDialog(null,"Nilai Anda "+ total + " : C");
        }
        /* jika nilai selaiN diatas yang disini adalah kurang dari 50 */
        else
        {
            JOptionPane.showMessageDialog(null,"Nilai Anda " + total+" : D");
        }
    }
}
