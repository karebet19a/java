package com.example;

public class Nim
{
    /**
     * @author fajar subhan
     * @return string
     * @description determine odd and even numbers nim
     *
     * */
    public static void main(String[] args)
    {
        /**
         * @notes : bilangan ganjil adalah bilangan yang tidak habis jika dibagi 2
         * @notes : bilangan genap adalah bilangan yang akan habis jika dibagi 2
         *
         * */

        // deklrasikan variable and inisialisasikan variable
        long nim =  202043500578L;

        // bilangan genap
        if(nim % 2 == 0)
        {
            //Jika npm kalian genap maka angka yang dimulai dari 2 sampai dengan 100
            for(int i = 2; i <= 100;i++)
            {
                System.out.println("Nim genap");
                System.out.println("Angka >>> "+i);
            }
        }
        // bilangan ganjil
        else
        {
            //Jika npm kalian ganjil maka angka yang dimulai dari 1 sampai dengan 99
            for(int j = 1;j < 100; j++)
            {
                System.out.println("Nim ganjil");
                System.out.println("Angka >>> " + j);
            }
        }


    }
}
